<?php
error_reporting(E_ERROR);

include 'cache.php';
include 'Logger.php';

function dump($var) {
    echo '<pre>' . print_r($var, true) . '</pre>';
}

$cache_path = __DIR__  . '/cache';

$pdo = new PDO('sqlite:db.sqlite3');

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$logger = new Logger();

$pdo->exec('
CREATE TABLE IF NOT EXISTS users (
  uuid VARCHAR(255) NOT NULL,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  phone VARCHAR(255) DEFAULT NULL,
  email VARCHAR(255) DEFAULT NULL,
  address TEXT NOT NULL,
  registered_at DATETIME NOT NULL,
  PRIMARY KEY (uuid)
)
');



$action = $_GET['action'];

if (! $action) {
    $action = 'load_from_file';
}

if ($action == 'remove_from_db') {

    $uuid = $_GET['uuid'];

    $pdo->exec('DELETE FROM users WHERE uuid = "' . $uuid . '"');

    Cache::reset('db');

    $logger->log('Remove from db ' . $uuid);

    header("Location: /?action=load_from_db");
    exit();
}

if ($action == 'remove_from_file') {

    $uuid = $_GET['uuid'];

    $users = unserialize(file_get_contents('users'));
    unset($users[$uuid]);
    file_put_contents(__DIR__ . '/users', serialize($users));

    $logger->log('Remove from file ' . $uuid);

    header("Location: /?action=load_from_file");
    exit();
}

if ($action == 'generate_db') {
    foreach (json_decode(file_get_contents('https://randomuser.me/api/?results=5&nat=gb'), true)['results'] as $data) {
        $uuid = uniqid();
        $users[$uuid] = [
            'first_name' =>  $data['name']['first'],
            'last_name' =>  $data['name']['last'],
            'location' => $data['location'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'registered_at' => date_create($data['registered']),
        ];
    }


    $pdo->exec('DELETE FROM users');

    foreach ($users as $uuid => $user) {
        $serializedLocation = json_encode($user['location']);
        $pdo->exec("INSERT INTO users (uuid, first_name, last_name, email, phone, address, registered_at) VALUES 
          (
            '$uuid', 
            '{$user['first_name']}', 
            '{$user['last_name']}', 
            '{$user['email']}', 
            '{$user['phone']}', 
            '{$serializedLocation}', 
            '{$user['registered_at']->format('Y-m-d H:i:s')}'
        )");
    }

    Cache::reset('db');

    $logger->log('Fill db');

    header("Location: /?action=load_from_db");
    exit();
}


if ($action == 'generate_file') {
    foreach (json_decode(file_get_contents('https://randomuser.me/api/?results=5&nat=gb'), true)['results'] as $data) {
        $uuid = uniqid();
        $users[$uuid] = [
            'uuid' => $uuid,
            'first_name' =>  $data['name']['first'],
            'last_name' =>  $data['name']['last'],
            'location' => $data['location'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'registered_at' => date_create($data['registered']),
        ];
    }

    file_put_contents(__DIR__ . '/users', serialize($users));

    $logger->log('Fill file');

    header("Location: /?action=load_from_file");
    exit();
}

if ($action == 'load_from_file') {
    $users = unserialize(file_get_contents('users'));
    $logger->log('Load from file');
} elseif ($action == 'load_from_db') {
    $users = Cache::get('db');

    if ($users === null) {
        foreach ($pdo->query('SELECT * FROM users')->fetchAll() as $row) {
            $users[$row['uuid']] = [
                'uuid' => $row['uuid'],
                'first_name' =>  $row['first_name'],
                'last_name' =>  $row['last_name'],
                'location' => json_decode($row['address'], true),
                'email' => $row['email'],
                'phone' => $row['phone'],
                'registered_at' => date_create($row['registered_at']),
            ];
        }
        if (! $users) {
            $users = [];
        }
        Cache::set('db', $users);
        $logger->log('Load from db');
    } else {
        $logger->log('Load from cache');
    }
}


?>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a class="nav-link <?= ($action == 'load_from_file' ? 'active' : '')?>" href="/?action=load_from_file">Файл</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= ($action == 'load_from_db' ? 'active' : '')?>" href="/?action=load_from_db">База данных</a>
                </li>
            </ul>
        </nav>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">Телефон</th>
                <th scope="col">Email</th>
                <th scope="col">Адрес</th>
                <th scope="col">Зарегистрирован</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $number = 1;
            foreach ($users as $user) {
                $date = $user['registered_at'];
                $monthes = [
                        '01' => 'января',
                        '02' => 'февраля',
                        '03' => 'марта',
                        '04' => 'апреля',
                        '05' => 'мая',
                        '06' => 'июня',
                        '07' => 'июля',
                        '08' => 'августа',
                        '09' => 'сентября',
                        '10' => 'октября',
                        '11' => 'ноября',
                        '12' => 'декабря',
                ];
                $month = $monthes[$user['registered_at']->format('m')];

                ?>
                <tr>
                    <th scope="row"><?= $number++ ?></th>
                    <td><?= $user['first_name']?></td>
                    <td><?= $user['last_name']?></td>
                    <td><?= $user['phone']?></td>
                    <td><?= $user['email']?></td>
                    <td><?= implode(', ', $user['location']) ?></td>
                    <td><?= $date->format('d ' . $month . ' Y H:i:s') ?></td>
                    <td>
                        <?php if ($action == 'load_from_db') {?>
                            <a href="/?action=remove_from_db&uuid=<?= $user['uuid']?>" class="btn btn-danger">Удалить</a>
                        <?php } else {?>
                            <a href="/?action=remove_from_file&uuid=<?= $user['uuid']?>" class="btn btn-danger">Удалить</a>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <div class="container-fluid">
            <?php if ($action == 'load_from_db') {?>
                <a class="btn btn-success float-right" href="/?action=generate_db" role="button">Сгенерировать в базе</a>
            <?php } else {?>
                <a class="btn btn-success float-right" href="/?action=generate_file" role="button">Сгенерировать в файле</a>
            <?php } ?>
        </div>


    </body>
</html>

